package api.document.messaging

import api.document.constant.KairosConstant
import api.document.models.KairosMessage
import api.document.models.KairosMessageAndResponseDTO
import api.document.producer.KairosProducer
import api.document.producer.KairosProducerImpl
import org.springframework.amqp.rabbit.core.RabbitTemplate
import spock.lang.Specification

class  KairosProducerSpec extends Specification {

    KairosProducer service
    RabbitTemplate kairosRabbitTemplate
    KairosMessage kairosMessage
    Map<String, Object> map

   def setup(){
        kairosRabbitTemplate = Mock()
        service = new KairosProducerImpl(kairosRabbitTemplate: kairosRabbitTemplate)

        kairosMessage = new KairosMessage(
                kairosValidationDTO: new KairosMessageAndResponseDTO(),
                maxAttempts: 2
        )
       map = new HashMap<>()
    }

    void 'should send a message to donwload' (){
        when:
        service.message(kairosMessage,  KairosConstant.DOWNLOAD_QUEUE)
        then:
        1 * kairosRabbitTemplate.convertAndSend(_,_)
    }

    void 'should send a message to enroll' (){
        when:
        service.message(kairosMessage,  KairosConstant.ENROLL_QUEUE)
        then:
        1 * kairosRabbitTemplate.convertAndSend(_,_)
    }

    void 'should send a message to recognize' (){
        when:
        service.message(kairosMessage, KairosConstant.RECOGNIZE_QUEUE)
        then:
        1 * kairosRabbitTemplate.convertAndSend(_,_)
    }

    void 'should send a message to last queue' (){
        when:
        service.messageLast(map)
        then:
        1 * kairosRabbitTemplate.convertAndSend(_,_)
    }
}