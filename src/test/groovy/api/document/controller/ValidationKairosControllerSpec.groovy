package api.document.controller

import api.document.models.KairosFaceDetecDTO
import api.document.models.ResponseEnroledToGalleryDTO
import api.document.models.ResponseValidationKairosDTO
import api.document.repositories.KairosFaceDetectRepository
import api.document.service.ValidationKairosService
import mx.com.kubofinanciero.onboarding.common.dto.load.KairosValidationDTO
import org.springframework.http.HttpStatus
import spock.lang.Specification

class ValidationKairosControllerSpec extends Specification {

    ValidationKairosController controllerOnService
    KairosFaceDetectRepository repository
    ValidationKairosController controller
    ValidationKairosService service
    List<KairosFaceDetecDTO> faceDetecDTOList
    ResponseValidationKairosDTO responseValidationKairos
    KairosValidationDTO requestKairosValidationDTO
    List<KairosValidationDTO> listRequestKairosValidation = new ArrayList<>()

    def setup() {
        repository = Mock()
        controller = new ValidationKairosController(repository: repository)
        service = Mock()
        faceDetecDTOList = [new KairosFaceDetecDTO()]
        controllerOnService = new ValidationKairosController(service: service)
        responseValidationKairos = new ResponseValidationKairosDTO()
        requestKairosValidationDTO = new KairosValidationDTO(845, 240 , 675 , "845/675/ife_front_resource_provider_675_845.png")
        listRequestKairosValidation.add(requestKairosValidationDTO)
    }

    def 'should return a kairosFaceDetec dto list' () {
        when:
        def faceDetecDTOList = controller.getProspect(845).getBody() as List
        then:
        1 * repository.findByProspectusId(845, null) >> faceDetecDTOList
        faceDetecDTOList.size() == 1
    }

    def 'should return a list of kairosFaceDetec dto shortened by loan' () {
        when:
        def faceDetecDTOList = controller.getProspectusLoan(845, 1).getBody() as List
        then:
        1 * repository.findByProspectusId(845, 1) >> faceDetecDTOList
        faceDetecDTOList.size() == 1
    }

    def 'should return a successful response' (){
        when:
        def responseValidationKairos = controllerOnService.kairosValidation(listRequestKairosValidation).getBody() as ResponseValidationKairosDTO
        then:
        responseValidationKairos.success == (Boolean) [true]
        and:
        1 * service.sendMessageToProcess(_) >> ''
    }

    def 'should return a successful response in enroll' (){
        when:
        def responseEnroledToGalleryDTO = controllerOnService.enroll(requestKairosValidationDTO)
        then:
        responseEnroledToGalleryDTO.getStatusCode() ==  HttpStatus.OK
        and:
        1 * service.enrrollToGallery(_,[:]) >> new ResponseEnroledToGalleryDTO(true, "Prueba Frau", "Successfully enrolled in the gallery Prueba Frau")
    }
}