package api.document.services

import api.document.models.DetailDownloadDTO
import api.document.models.DetailEnrollRecognizeDTO
import api.document.models.KairosMessage
import api.document.models.KairosMessageAndResponseDTO
import api.document.models.KairosValidationRequestDTO
import api.document.models.ResponseDetailsDTO
import api.document.models.ResponseEnroledToGalleryDTO
import api.document.producer.KairosProducer
import api.document.service.impl.ValidationKairosServiceImpl
import mx.com.kubofinanciero.client.aws.helper.HelperClient
import mx.com.kubofinanciero.client.aws.service.AwsBucketClient
import mx.com.kubofinanciero.client.kairos.converter.KairosConverter
import mx.com.kubofinanciero.client.kairos.service.KairosClient
import mx.com.kubofinanciero.onboarding.common.dto.kairos.KairosFaceDetectDTO
import mx.com.kubofinanciero.onboarding.common.dto.load.KairosValidationDTO
import mx.com.kubofinanciero.service.BinnacleForKairosClient
import mx.com.kubofinanciero.service.DocumentInformationClient
import org.apache.commons.io.IOUtils
import org.codehaus.jackson.map.ObjectMapper
import org.springframework.amqp.core.Message
import spock.lang.Specification

class ValidationKairosServiceImplSpec extends Specification {
    ValidationKairosServiceImpl service

    AwsBucketClient awsBucketClient
    BinnacleForKairosClient binnacleForKairosClient
    DocumentInformationClient documentInformationClient
    HelperClient helperClient
    KairosClient kairosClient
    KairosConverter kairosConverter
    KairosProducer kairosProducer
    Message msg

    KairosValidationDTO requestKairosValidationDTO
    List<KairosValidationDTO> listRequestKairosValidation = new ArrayList<>()
    String id
    ResponseEnroledToGalleryDTO responseEnroledToGalleryDTO
    KairosMessage kairosMessage
    KairosMessageAndResponseDTO kairosMessageAndResponseDTO
    List<KairosValidationRequestDTO> listKairosValidationRequestDTO = new ArrayList<>()
    KairosValidationRequestDTO kairosValidationRequestDTO
    ResponseDetailsDTO responseDetailsDTO
    ObjectMapper objectMapper
    KairosFaceDetectDTO kairosFaceDetectDTO

    def setup() {
        requestKairosValidationDTO = new KairosValidationDTO(845, 240 , 675 , "845/675/ife_front_resource_provider_675_845.png")
        listRequestKairosValidation.add(requestKairosValidationDTO)
        id = ""

        responseDetailsDTO = new ResponseDetailsDTO()
        responseDetailsDTO.setDonwload(new DetailDownloadDTO("filename-test", "1m", true, 1, "ok-test", "date-test"))
        responseDetailsDTO.setEnroll(new DetailEnrollRecognizeDTO("Gallery-test", true, 1, "Erolled-test", "date-test"))

        kairosValidationRequestDTO = new KairosValidationRequestDTO()
        kairosValidationRequestDTO.setProspectId(845)
        kairosValidationRequestDTO.setFileTypeId(240)
        kairosValidationRequestDTO.setProjectLoanId(675)
        kairosValidationRequestDTO.setFileLocation("845/675/ife_front_resource_provider_675_845.png")
        kairosValidationRequestDTO.setResponse(new ArrayList<KairosFaceDetectDTO>())
        kairosValidationRequestDTO.setMapAreApprovedOverGallery([:])
        kairosValidationRequestDTO.setDetails(responseDetailsDTO)
        kairosValidationRequestDTO.setNameAdaptationForKairos("localnaming")

        listKairosValidationRequestDTO.add(kairosValidationRequestDTO)

        kairosMessageAndResponseDTO = new KairosMessageAndResponseDTO()
        kairosMessageAndResponseDTO.setUuid("id-test")
        kairosMessageAndResponseDTO.setElements(listKairosValidationRequestDTO)
        kairosMessageAndResponseDTO.setNumber_elements(1)
        kairosMessageAndResponseDTO.setMessage("test")
        kairosMessageAndResponseDTO.setInitial_time("1")
        kairosMessageAndResponseDTO.setFinish_time("2")
        kairosFaceDetectDTO = new KairosFaceDetectDTO()
        kairosFaceDetectDTO.setStatus("success")


        kairosMessage = new KairosMessage(
                kairosValidationDTO: kairosMessageAndResponseDTO,
                maxAttempts: 2
        )
        objectMapper = new ObjectMapper()
        msg = new Message(objectMapper.writeValueAsBytes(kairosMessage))

        awsBucketClient = Mock()
        binnacleForKairosClient = Mock()
        documentInformationClient = Mock()
        helperClient = Mock()
        kairosClient = Mock()
        kairosConverter = Mock()
        kairosProducer = Mock()

        service = new ValidationKairosServiceImpl()
                service.awsBucketClient = awsBucketClient
                service.binnacleForKairosClient =  binnacleForKairosClient
                service.documentInformationClient = documentInformationClient
                service.helperClient = helperClient
                service.kairosClient = kairosClient
                service.kairosConverter = kairosConverter
                service.kairosProducer = kairosProducer
                service.recognizeIdentifiers = [1, 119, 240, 242, 243, 245]
                service.LOCAL_STORAGE_DYNAMIC_PATH = 'test-mock-unused'
        service.enroll_gallery = 'Prueba Test'
        service.galleryFraudulent = 'Prueba Test Frau'

        responseEnroledToGalleryDTO = new ResponseEnroledToGalleryDTO(true, "Prueba Frau", "Successfully enrolled in the gallery Prueba Frau")
    }

    void 'should return id String value '() {
        when:
        String id = service.sendMessageToProcess(listRequestKairosValidation)
        then:
        id.getClass() == String.class
        and:
        1 * kairosProducer.message(_,_)
    }

    def 'must return an enroll object'(){
        when:
        ResponseEnroledToGalleryDTO responseEnroledToGalleryDTO = service.enrrollToGallery(requestKairosValidationDTO, [:])
        then:
        1 * awsBucketClient.getPreSignedUrlByFilename(_) >> 'https://kubo-documentos-qa.s3.us-east-2.amazonaws.com/845/675/ife_front_resource_provider_675_845_test.png'
        1 * helperClient.dowloadFromUrlToInputStream(_,_,_) >> IOUtils.toInputStream("some test data for my input stream", "UTF-8")
        1 * helperClient.appropriateSizeImage(_)
        1 * helperClient.fileToBase64(_,_) >> new File("src/test/resources/base64.txt").text
        1 * kairosClient.enroll(_,_,_)
        responseEnroledToGalleryDTO == responseEnroledToGalleryDTO
    }

    void 'should listen and send a message download'(){
        when:
        service.handleMsgDownload(msg)
        then:
        1 * awsBucketClient.getPreSignedUrlByFilename(_) >> 'https://kubo-documentos-qa.s3.us-east-2.amazonaws.com/845/675/ife_front_resource_provider_675_845_test.png'
        1 * helperClient.dowloadFromUrlToInputStream(_,_,_) >> IOUtils.toInputStream("some test data for my input stream", "UTF-8")
        1 * kairosProducer.message(_,_)
        notThrown()
    }

    void 'should listen and send a message enroll'(){
        when:
        service.handleMsgEnroll(msg)
        then:
        1 * helperClient.appropriateSizeImage(_)
        1 * helperClient.fileToBase64(_,_) >> new File("src/test/resources/base64.txt").text
        1 * kairosClient.enroll(_,_,_)
        1 * binnacleForKairosClient.saveRecord(_,_,_,_)
        1 * kairosProducer.message(_,_)
        notThrown()
    }

    void 'should listen and send a message recognize'(){
        when:
        service.handleMsgRecognize(msg)
        then:
        1 * kairosClient.getGalleries() >> ['test']
        1 * helperClient.appropriateSizeImage(_)
        1 * helperClient.fileToBase64(_,_) >> new File("src/test/resources/base64.txt").text
        1 * kairosClient.recognize(_,_,_) >> new HashMap<>()
        1 * binnacleForKairosClient.saveRecord(_,_,_,_)
        1 * kairosConverter.fromRecognizeMap(_,_,_,_,_,_) >> kairosFaceDetectDTO
        1 * documentInformationClient.saveKairos(_,_,_)
        1 * kairosProducer.messageLast(_)
        notThrown()
    }
}