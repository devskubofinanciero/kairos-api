package api.document.exceptions

import groovy.transform.CompileStatic
import org.springframework.http.HttpStatus
import org.springframework.web.server.ResponseStatusException

@CompileStatic
class KairosGetException extends ResponseStatusException {
    KairosGetException(String message) {
        super(HttpStatus.NOT_FOUND, message)
    }
}
