package api.document.exceptions

import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.ResponseStatus

@ResponseStatus(code = HttpStatus.BAD_REQUEST)
class KairosFaceNotRecognizeException extends RuntimeException {
    KairosFaceNotRecognizeException(String message) {
        super(message)
    }
}
