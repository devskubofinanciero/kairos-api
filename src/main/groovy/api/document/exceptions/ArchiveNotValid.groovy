package api.document.exceptions

import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.ResponseStatus

@ResponseStatus(code = HttpStatus.BAD_REQUEST, reason = "Archive not allow")
class ArchiveNotValid extends RuntimeException {
    ArchiveNotValid() {
    }
}