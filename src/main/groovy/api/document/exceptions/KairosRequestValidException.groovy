package api.document.exceptions

import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.ResponseStatus

@ResponseStatus(code = HttpStatus.BAD_REQUEST)
class KairosRequestValidException extends RuntimeException {
    KairosRequestValidException(String message) {
        super(message)
    }
}