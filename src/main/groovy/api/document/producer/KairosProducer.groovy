package api.document.producer

import api.document.models.KairosMessage
import api.document.models.KairosMessageAndResponseDTO

interface KairosProducer {
    void message(KairosMessage body, int EnrollOrRecognize)
    void messageLast(Map<String, Object> body)
}
