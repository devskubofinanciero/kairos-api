package api.document.producer

import api.document.models.KairosMessage
import groovy.util.logging.Slf4j
import org.springframework.amqp.rabbit.core.RabbitTemplate
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Service

@Slf4j
@Service
class KairosProducerImpl implements KairosProducer {

    @Autowired
    @Qualifier("kairosRabbitTemplate")
    private RabbitTemplate kairosRabbitTemplate
    @Value('${kubo.onboarding.kairos.queue}')
    private String QUEUE
    @Value('${kubo.onboarding.kairos.queue.enroll}')
    private String QUEUE_ENROLL
    @Value('${kubo.onboarding.kairos.queue.recognize}')
    private String QUEUE_RECOGNIZE
    @Value('${kubo.onboarding.kairos.queue.result}')
    private String QUEUE_LAST

    @Override
    void message(KairosMessage body, int EnrollOrRecognize) {
        if(EnrollOrRecognize == 0){
            kairosRabbitTemplate.convertAndSend(QUEUE, body)
        }
        else if(EnrollOrRecognize == 1) {
            kairosRabbitTemplate.convertAndSend(QUEUE_ENROLL, body)
        }else{
            kairosRabbitTemplate.convertAndSend(QUEUE_RECOGNIZE, body)
        }
    }

    @Override
    void messageLast(Map<String, Object> body) {
        kairosRabbitTemplate.convertAndSend(QUEUE_LAST, body)
    }
}