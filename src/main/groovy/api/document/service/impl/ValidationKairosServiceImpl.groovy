package api.document.service.impl

import api.document.constant.KairosConstant
import api.document.exceptions.ArchiveNotValid

import api.document.exceptions.KairosFaceNotRecognizeException
import api.document.models.DetailDownloadDTO
import api.document.models.DetailEnrollRecognizeDTO
import api.document.models.KairosMessage
import api.document.models.KairosMessageAndResponseDTO
import api.document.models.KairosValidationRequestDTO
import api.document.models.ResponseDetailsDTO
import api.document.models.ResponseEnroledToGalleryDTO
import api.document.producer.ClienteKairosASL
import api.document.producer.KairosProducer
import api.document.service.ValidationKairosService
import com.fasterxml.jackson.databind.ObjectMapper
import groovy.json.JsonSlurper
import groovy.util.logging.Slf4j
import mx.com.kubofinanciero.client.aws.constant.AWSConstants
import mx.com.kubofinanciero.client.aws.helper.HelperClient
import mx.com.kubofinanciero.client.aws.service.AwsBucketClient
import mx.com.kubofinanciero.client.kairos.converter.KairosConverter
import mx.com.kubofinanciero.client.kairos.exceptions.KairosApiException
import mx.com.kubofinanciero.client.kairos.exceptions.KairosErrorOfImageException
import mx.com.kubofinanciero.client.kairos.exceptions.KairosFaceNotFoundException
import mx.com.kubofinanciero.client.kairos.exceptions.KairosPayloadException
import mx.com.kubofinanciero.client.kairos.service.KairosClient
import mx.com.kubofinanciero.exception.ClientOnboardingInformationException
import mx.com.kubofinanciero.onboarding.common.dto.kairos.KairosFaceDetectDTO
import mx.com.kubofinanciero.onboarding.common.dto.load.KairosValidationDTO
import mx.com.kubofinanciero.service.BinnacleForKairosClient
import mx.com.kubofinanciero.service.DocumentInformationClient
import org.apache.commons.validator.routines.UrlValidator
import org.glassfish.jersey.message.internal.MessageBodyProviderNotFoundException
import org.springframework.amqp.core.Message
import org.springframework.amqp.rabbit.annotation.RabbitListener
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Service

import java.nio.charset.StandardCharsets
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter


@Slf4j
@Service
class ValidationKairosServiceImpl implements ValidationKairosService {

    @Autowired
    private AwsBucketClient awsBucketClient
    @Autowired
    private BinnacleForKairosClient binnacleForKairosClient
    @Autowired
    private DocumentInformationClient documentInformationClient
    @Autowired
    private HelperClient helperClient
    @Autowired
    private KairosClient kairosClient
    @Autowired
    private KairosConverter kairosConverter
    @Autowired
    private KairosProducer kairosProducer

    ////////// prueba cliente local
    @Autowired
    private ClienteKairosASL clienteKairosASL

    @Autowired
    @Qualifier("keysOfDocuments")
    List recognizeIdentifiers

    @Value('${kubo.storage.local.dynamic.path}')
    String LOCAL_STORAGE_DYNAMIC_PATH
    String KAIROS_RETRY = 'kairos_retry_'
    @Value('${kairos.app.gallery.enroll}')
    String enroll_gallery
    @Value('${kairos.app.gallery.enroll}')
    String galleryFraudulent

    @Override
    String sendMessageToProcess(List<KairosValidationDTO> request) {
        KairosMessageAndResponseDTO dtoRequest = createDto(request)
        kairosProducer.message(new KairosMessage(kairosValidationDTO : dtoRequest,
                maxAttempts : KairosConstant.MAX_ATTEMPTS_NUMBER), KairosConstant.DOWNLOAD_QUEUE)
        return dtoRequest.getUuid()
    }

    @Override
    ResponseEnroledToGalleryDTO enrrollToGallery(KairosValidationDTO request, Map mapAreApprovedOverGallery) {
        String localName = (request.getFileLocation().split("\\/") as List<String>).get(2)
        String nameAdaptationForKairos = KAIROS_RETRY + localName
        String messageEnroll
        boolean enrolled
        try {
            String awsfile = awsBucketClient.getPreSignedUrlByFilename(request.getFileLocation())
            downloadFile(awsfile, nameAdaptationForKairos)
            Map<String, Object> responseValidationAndEnroll = this.validationAndEnroll(request, nameAdaptationForKairos, mapAreApprovedOverGallery)
            enrolled = responseValidationAndEnroll.get("enroll")
            messageEnroll = "Successfully enrolled in the gallery " + galleryFraudulent
        } catch (Exception e) {
            enrolled = false
            messageEnroll = e.getMessage().toString()
        }
        return new ResponseEnroledToGalleryDTO(enrolled, galleryFraudulent, messageEnroll)
    }

    @SuppressWarnings("unchecked")
    @RabbitListener(queues = "kubo.onboarding.document.kairos")
    void handleMsgDownload(Message msg) {
        Map<String, Object> json = convertByteToMap(msg)
        KairosMessageAndResponseDTO dto = json.get(KairosConstant.KAIROS_VALIDATION_DTO) as KairosMessageAndResponseDTO
        this.processDownloadFiles(dto)
    }

    @SuppressWarnings(["unchecked", 'GroovyAssignabilityCheck'])
    private void processDownloadFiles(KairosMessageAndResponseDTO kairosDto) {
        List<KairosValidationRequestDTO> listElementsDTO = kairosDto.getElements()
        List<KairosValidationRequestDTO> newListElementsDTO = new ArrayList<>()

        for (KairosValidationRequestDTO dto : listElementsDTO) {
            long initialTime =  System.currentTimeMillis()
            String localName = (dto.getFileLocation().split("\\/") as List<String>).get(2)
            String nameAdaptationForKairos = KAIROS_RETRY + localName
            ResponseDetailsDTO detailsDto = new ResponseDetailsDTO()
            DetailDownloadDTO detailDownload
            KairosValidationRequestDTO newDtoKairosValidationRequestDTO
            long finalTime
            long difTime
            try {
                downloadFile(awsBucketClient.getPreSignedUrlByFilename(dto.getFileLocation()), nameAdaptationForKairos)
                newDtoKairosValidationRequestDTO = genericGenerateDto(dto, [:], nameAdaptationForKairos)
                finalTime = System.currentTimeMillis()
                detailDownload = new DetailDownloadDTO(nameAdaptationForKairos,getlengthFile(LOCAL_STORAGE_DYNAMIC_PATH, nameAdaptationForKairos) , true, finalTime-initialTime, "The image was successfully downloaded in "+ finalTime-initialTime + " milliseconds", getDate())
            } catch (Exception e) {
                newDtoKairosValidationRequestDTO = genericGenerateDto(dto, [:], nameAdaptationForKairos)
                finalTime = System.currentTimeMillis()
                detailDownload = new DetailDownloadDTO(nameAdaptationForKairos,getlengthFile(LOCAL_STORAGE_DYNAMIC_PATH, nameAdaptationForKairos) , false, finalTime-initialTime, e.getMessage().toString(), getDate())
            }
            detailsDto.setDonwload(detailDownload)
            newDtoKairosValidationRequestDTO.setDetails(detailsDto)
            newListElementsDTO.add(newDtoKairosValidationRequestDTO)
        }
        kairosDto.setElements(newListElementsDTO)
       kairosProducer.message(new KairosMessage(kairosValidationDTO : kairosDto,
               maxAttempts : KairosConstant.MAX_ATTEMPTS_NUMBER), KairosConstant.ENROLL_QUEUE)
    }

    @RabbitListener(queues = "kubo.onboarding.document.kairos.enroll")
    void handleMsgEnroll(Message msg) {
        Map<String, Object> json = convertByteToMap(msg)
        KairosMessageAndResponseDTO dtoJson = json.get(KairosConstant.KAIROS_VALIDATION_DTO) as KairosMessageAndResponseDTO
        List<KairosValidationRequestDTO> listElementsDTO = dtoJson.getElements()
        List<KairosValidationRequestDTO> newListElementsDTO = new ArrayList<>()

        listElementsDTO.each { map ->
            long initialTime =  System.currentTimeMillis()
            int prospectId = map["prospectId"] as int
            int fileTypeId = map["fileTypeId"] as int
            int projectLoanId = map["projectLoanId"] as int
            String fileLocation = map["fileLocation"]
            String nameAdaptationForKairos = map[KairosConstant.NAME_ADAPTATION_FOR_KAIROS]
            ResponseDetailsDTO responseDetailsDTO = map["details"] as ResponseDetailsDTO
            DetailDownloadDTO downloadDetailDTO = responseDetailsDTO.getDonwload()
            Map<String, Object> mapAreApprovedOverGallery = map[KairosConstant.MAP_ARE_APPROVED_OVER_GALLERY] as Map<String, Object>
            KairosValidationRequestDTO newDtoKairosValidationRequestDTO
            long finalTime
            DetailEnrollRecognizeDTO detailEnrollDto
            boolean isEnroll = false
            if (downloadDetailDTO.getIs_success()){
                KairosValidationDTO kairosValidationDTO = new KairosValidationDTO(prospectId, fileTypeId, projectLoanId, fileLocation)
                try {
                    isEnroll = this.enrollByQueue(kairosValidationDTO, nameAdaptationForKairos, mapAreApprovedOverGallery as Map<String, Boolean>, isEnroll)
                    finalTime = System.currentTimeMillis()
                    detailEnrollDto = new DetailEnrollRecognizeDTO(enroll_gallery, isEnroll, finalTime-initialTime, "Enrolled in the gallery " + enroll_gallery +" in " + finalTime-initialTime + " milliseconds Successfully", getDate())
                } catch (Exception e) {
                    finalTime = System.currentTimeMillis()
                    detailEnrollDto = new DetailEnrollRecognizeDTO(enroll_gallery, isEnroll, finalTime-initialTime, e.getMessage().toString(), getDate())
                }
                responseDetailsDTO.setEnroll(detailEnrollDto)
                newDtoKairosValidationRequestDTO = genericGenerateDto(map as KairosValidationRequestDTO, mapAreApprovedOverGallery, nameAdaptationForKairos)
                newDtoKairosValidationRequestDTO.setDetails(responseDetailsDTO)
            } else {
                finalTime = System.currentTimeMillis()
                detailEnrollDto = new DetailEnrollRecognizeDTO(enroll_gallery, isEnroll, finalTime-initialTime, "No image downloaded", getDate())
                responseDetailsDTO.setEnroll(detailEnrollDto)
                newDtoKairosValidationRequestDTO = genericGenerateDto(map as KairosValidationRequestDTO, mapAreApprovedOverGallery, nameAdaptationForKairos)
                newDtoKairosValidationRequestDTO.setDetails(responseDetailsDTO)
            }
            newListElementsDTO.add(newDtoKairosValidationRequestDTO)
        }
        dtoJson.setElements(newListElementsDTO)

        kairosProducer.message(new KairosMessage(kairosValidationDTO : dtoJson,
                maxAttempts : KairosConstant.MAX_ATTEMPTS_NUMBER), KairosConstant.RECOGNIZE_QUEUE)
    }

    private boolean enrollByQueue(KairosValidationDTO dto, String localNaming, Map<String, Boolean> mapAreApprovedOverGallery, boolean isEnroll) {
        if (recognizeIdentifiers.contains(dto.getFileTypeId())) {
            isEnroll = true
            if (!mapAreApprovedOverGallery.get(KairosConstant.ENROLL_TO_GALLERY)) {
                isEnroll = this.processEnroll(dto.getProspectId(), dto.getProjectLoanId(), localNaming)

                mapAreApprovedOverGallery.put(KairosConstant.ENROLL_TO_GALLERY, isEnroll)
            }
            if (!isEnroll) {
                throw new KairosFaceNotRecognizeException("fileLocation ${localNaming} faces not found - mapAreApprovedOverGallery ${mapAreApprovedOverGallery.toString()}")
            }
        } else {
           throw new KairosFaceNotRecognizeException("FileTypeId identifier is not recognized")
        }
        return isEnroll
    }

    @RabbitListener(queues = "kubo.onboarding.document.kairos.recognize")
    void handleMsgRecognize(Message msg) {
        Map<String, Object> json = convertByteToMap(msg)
        KairosMessageAndResponseDTO dtoJson = json.get(KairosConstant.KAIROS_VALIDATION_DTO) as KairosMessageAndResponseDTO
        List<KairosValidationRequestDTO> listElementsDTO = dtoJson.getElements()
        List<KairosValidationRequestDTO> newListElementsDTO = new ArrayList()
        KairosValidationRequestDTO newDtoKairosValidationRequestDTO
        ObjectMapper objectMapper = new ObjectMapper()

        listElementsDTO.each { map ->
            int prospectIdRec = map["prospectId"] as int
            int fileTypeIdRec = map["fileTypeId"] as int
            int projectLoanIdRec = map["projectLoanId"] as int
            String fileLocationRec = map["fileLocation"]
            String nameAdaptationForKairosRec = map[KairosConstant.NAME_ADAPTATION_FOR_KAIROS]
            ResponseDetailsDTO responseDetailsDTO = map["details"] as ResponseDetailsDTO
            Map<String, Object> mapAreApprovedOverGalleryRec = map[KairosConstant.MAP_ARE_APPROVED_OVER_GALLERY] as Map<String, Object>
            long initialTime = System.currentTimeMillis()
            long finalTime
            List<DetailEnrollRecognizeDTO> listDetailRecognize = new ArrayList<>()
            DetailEnrollRecognizeDTO detailRecognizeDTO
            List<KairosFaceDetectDTO> responseList = new ArrayList<>()

            if (responseDetailsDTO.getEnroll().is_success) {
                kairosClient.getGalleries().each { gallery ->
                    if (!mapAreApprovedOverGalleryRec.get(gallery)) {
                        initialTime = System.currentTimeMillis()
                        Map<String, Object> mapGalleryResponse = this.processGalleryV2(
                                prospectIdRec,
                                projectLoanIdRec,
                                fileTypeIdRec,
                                nameAdaptationForKairosRec,
                                fileLocationRec,
                                gallery)
                        mapAreApprovedOverGalleryRec.put(gallery, mapGalleryResponse.get("ok"))
                        mapGalleryResponse.remove("ok")
                        finalTime = System.currentTimeMillis()
                        newDtoKairosValidationRequestDTO = genericGenerateDto(map as KairosValidationRequestDTO, mapAreApprovedOverGalleryRec, nameAdaptationForKairosRec)
                        KairosFaceDetectDTO response = mapGalleryResponse.get("response") as KairosFaceDetectDTO
                        if ("success" != response.getStatus()) {
                            Map<String, Object> mapResponse = objectMapper.readValue(response.getResponse(), Map.class)
                            List<Map<String, Object>> lisMapResponseErrs = mapResponse["Errors"]
                            StringBuilder errors = new StringBuilder()
                            lisMapResponseErrs.each { mapErrors -> errors.append("ErrCode: ").append(mapErrors.get("ErrCode")).append(" Message: ").append(mapErrors.get("Message")).append("; ") }
                            detailRecognizeDTO = new DetailEnrollRecognizeDTO(gallery, false, finalTime - initialTime, errors.toString(), getDate())
                            listDetailRecognize.add(detailRecognizeDTO)
                        } else {
                            detailRecognizeDTO = new DetailEnrollRecognizeDTO(gallery, true, finalTime - initialTime, "It was successfully recognized in gallery "+ gallery + " in "+  finalTime - initialTime +" milliseconds", getDate())
                            listDetailRecognize.add(detailRecognizeDTO)
                        }
                        responseList.add(response)
                        newDtoKairosValidationRequestDTO.setResponse(responseList)
                        responseDetailsDTO.setRecognize(listDetailRecognize)
                        newDtoKairosValidationRequestDTO.setDetails(responseDetailsDTO)
                    }
                }
                newListElementsDTO.add(newDtoKairosValidationRequestDTO)
            } else {
                newDtoKairosValidationRequestDTO = genericGenerateDto(map as KairosValidationRequestDTO, mapAreApprovedOverGalleryRec, nameAdaptationForKairosRec)
                finalTime = System.currentTimeMillis()
                detailRecognizeDTO = new DetailEnrollRecognizeDTO("-", false, finalTime - initialTime, "No enroll", getDate())
                listDetailRecognize.add(detailRecognizeDTO)
                responseDetailsDTO.setRecognize(listDetailRecognize)
                newDtoKairosValidationRequestDTO.setDetails(responseDetailsDTO)
                newListElementsDTO.add(newDtoKairosValidationRequestDTO)
            }
        }
        dtoJson.setElements(newListElementsDTO)
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss")
        String timeFinal = dtf.format(LocalDateTime.now()).toString()
        dtoJson.setFinish_time(timeFinal)
        dtoJson.setMessage("OK")

        Map<String, Object> mapJsonResponse = objectMapper.convertValue(dtoJson, Map.class)
        List<Map<String, Object>> elementslist = (List<Map<String, Object>>) mapJsonResponse.get("elements")
        List<Map<String, Object>> elementslistNew = new ArrayList<>()

        elementslist.forEach({ m ->
            m.remove(KairosConstant.MAP_ARE_APPROVED_OVER_GALLERY)
            m.remove(KairosConstant.NAME_ADAPTATION_FOR_KAIROS)
            elementslistNew.add(m)
        })
        mapJsonResponse.put("elements", elementslistNew)
        /// Preguntar a DATA -- si esta bien dejar la información
        kairosProducer.messageLast(mapJsonResponse)
    }

    private void downloadFile(String archive, String localNaming) {
        String[] schemes = ['https']
        if (new UrlValidator(schemes).isValid(archive)) {
            try {
                helperClient.dowloadFromUrlToInputStream(LOCAL_STORAGE_DYNAMIC_PATH, archive, localNaming)
            }catch (Exception e){
                throw new RuntimeException(e.getMessage()+ " No valid")
            }
        } else {
            throw new ArchiveNotValid()
        }
    }

    private boolean processEnroll(Integer prospect, Integer loan, String localNaming) {
        boolean isEnroll = true
        helperClient.appropriateSizeImage(localNaming)
        String imageBase64 = helperClient.fileToBase64(LOCAL_STORAGE_DYNAMIC_PATH, localNaming)
        for (Integer attempt : [1, 2, 3, 4]) {
            try {
                long startTime = System.nanoTime()
                String subjectId = kairosClient.buildSubjectId(prospect, localNaming.replace(KAIROS_RETRY, ''))
                kairosClient.enroll(imageBase64, true, subjectId)
                binnacleForKairosClient.saveRecord(prospect, loan, (System.nanoTime() - startTime) / 1_000_000_000 as String, "kairosClient.enroll - subjectId ${subjectId} - localNaming ${localNaming}")
                isEnroll = true
                break
            } catch (KairosFaceNotFoundException | KairosApiException | KairosErrorOfImageException e) {
                isEnroll = false
                log.error "File ${localNaming} attemp ${attempt} ${e.getClass().getSimpleName()} ${e.getMessage()}"
                helperClient.rotate(localNaming, AWSConstants.ROTATE_LEFT, 1)
                imageBase64 = helperClient.fileToBase64(LOCAL_STORAGE_DYNAMIC_PATH, localNaming)
            }
        }
        isEnroll
    }

    private Map<String, Object> processEnrollV2(Integer prospect, Integer loan, String localNaming, boolean galleryEnroll) {
        Map<String, Object> response0 = new HashMap<>()
        String galleryOnEnroll = galleryEnroll == true ? enroll_gallery : galleryFraudulent
        helperClient.appropriateSizeImage(localNaming)
        String imageBase64 = helperClient.fileToBase64(LOCAL_STORAGE_DYNAMIC_PATH, localNaming)
        for (Integer attempt : [1, 2, 3, 4]) {
            try {
                String subjectId = kairosClient.buildSubjectId(prospect, localNaming.replace(KAIROS_RETRY, ''))
                //kairosClient.enrollToGallery(imageBase64, true, subjectId, galleryOnEnroll)
                // Para crear las pruebas unitarias
                // se apunto a enroll --> por que falta crear el metodo enrollToGallery en el cliente
                kairosClient.enroll(imageBase64, true, subjectId)
                //clienteKairosASL.enrollToGallery(imageBase64, true, subjectId, galleryOnEnroll)
                response0.put("enroll", true)
                response0.put("message", "Enrolled")
                break
            } catch (KairosFaceNotFoundException | KairosApiException | KairosErrorOfImageException e) {
                response0.put("enroll", false)
                response0.put("message", "File ${localNaming} attemp ${attempt} ${e.getClass().getSimpleName()} ${e.getMessage()}")
                log.error "File ${localNaming} attemp ${attempt} ${e.getClass().getSimpleName()} ${e.getMessage()}"
                helperClient.rotate(localNaming, AWSConstants.ROTATE_LEFT, 1)
                imageBase64 = helperClient.fileToBase64(LOCAL_STORAGE_DYNAMIC_PATH, localNaming)
            }
        }
        log.info("response0   ### {} ", response0)
        return response0
    }

    @SuppressWarnings(["unchecked", 'GroovyUncheckedAssignmentOfMemberOfRawType'])
    private Map<String, Object> processGalleryV2(Integer prospect, Integer loan, Integer fileTypeId, String localNaming, String cloudNaming, String gallery) {
        helperClient.appropriateSizeImage(localNaming)
        String imageBase64 = helperClient.fileToBase64(LOCAL_STORAGE_DYNAMIC_PATH, localNaming)
        Map<String, Object> response = new HashMap<>()
        try {
            long startTime = System.nanoTime()
            Map recognize = kairosClient.recognize(imageBase64, gallery, localNaming)
            binnacleForKairosClient.saveRecord(prospect, loan, (System.nanoTime() - startTime) / 1_000_000_000 as String, "kairosClient.recognize - gallery ${gallery} - localNaming ${localNaming}")
            KairosFaceDetectDTO kairosFaceDetectDTO = kairosConverter.fromRecognizeMap(prospect, loan, fileTypeId, recognize, cloudNaming, gallery)
            documentInformationClient.saveKairos(kairosFaceDetectDTO, prospect, loan)
            response.put("response", kairosFaceDetectDTO)
            response.put("ok", true)
        } catch (MessageBodyProviderNotFoundException | KairosApiException | KairosPayloadException | ClientOnboardingInformationException kairosException) {
            throw kairosException
        }
        response
    }


    private static Map<String, Object> convertByteToMap(Message msg) {
        byte[] bytes = msg.getBody()
        String str = new String(bytes, StandardCharsets.UTF_8)
        def slurper = new JsonSlurper()
        def json = slurper.parseText(str)
        assert json instanceof Map
        return json
    }

    private static KairosValidationRequestDTO genericGenerateDto (KairosValidationRequestDTO dto, Map<String, Object> mapAreApprovedOverGallery, String nameAdaptationForKairos){
        KairosValidationRequestDTO dtoResponse = new KairosValidationRequestDTO()
        dtoResponse.setFileTypeId(dto.getFileTypeId())
        dtoResponse.setProspectId(dto.getProspectId())
        dtoResponse.setFileLocation(dto.getFileLocation())
        dtoResponse.setProjectLoanId(dto.getProjectLoanId())
        dtoResponse.setNameAdaptationForKairos(nameAdaptationForKairos)
        dtoResponse.setMapAreApprovedOverGallery(mapAreApprovedOverGallery as Map<String, Boolean>)
        return dtoResponse
    }

    private static String getDate(){
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss")
        String timeInitial = dtf.format(LocalDateTime.now()).toString()
        return timeInitial
    }

    private static String getlengthFile(String path, String localFileName){
        File file = new File(path +localFileName)
        float megabytes = file.length() / KairosConstant.MEGABYTE
        return  String.format("%.3f", megabytes) + " MB"
    }

    private Map<String, Object> validationAndEnroll (KairosValidationDTO dto, String localNaming, Map<String, Boolean> mapAreApprovedOverGallery) {
        Map<String, Object> respoonseValidationEnroll = new HashMap<>()
            boolean isEnroll
            if (recognizeIdentifiers.contains(dto.getFileTypeId())) {
                isEnroll = true
                if (!mapAreApprovedOverGallery.get(KairosConstant.ENROLL_TO_GALLERY)) {
                    respoonseValidationEnroll = this.processEnrollV2(dto.getProspectId(), dto.getProjectLoanId(), localNaming, false)
                    isEnroll = respoonseValidationEnroll.get("enroll") as boolean
                    mapAreApprovedOverGallery.put(KairosConstant.ENROLL_TO_GALLERY, isEnroll)
                }
            } else {
                throw new KairosFaceNotRecognizeException("FileTypeId identifier is not recognized")
            }
        respoonseValidationEnroll.put("enroll", isEnroll)
        return respoonseValidationEnroll
    }

    protected KairosMessageAndResponseDTO createDto(List<KairosValidationDTO> request) {
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss")
        String timeInitial = dtf.format(LocalDateTime.now()).toString()
        String id = UUID.randomUUID().toString()
        KairosMessageAndResponseDTO dtoRequest = new KairosMessageAndResponseDTO()
        List<KairosValidationRequestDTO> listValidationDto = new ArrayList<>()
        DetailDownloadDTO dtoDownload = new DetailDownloadDTO()
        DetailEnrollRecognizeDTO dtoEnroll = new DetailEnrollRecognizeDTO()
        List<DetailEnrollRecognizeDTO> dtoRecognize = new ArrayList<>()
        ResponseDetailsDTO responseDetailsDto = new ResponseDetailsDTO()
        responseDetailsDto.setDonwload(dtoDownload)
        responseDetailsDto.setEnroll(dtoEnroll)
        responseDetailsDto.setRecognize(dtoRecognize)

        request.each({ dto ->
            api.document.models.KairosValidationRequestDTO validationDto = new KairosValidationRequestDTO()
            validationDto.setFileLocation(dto.getFileLocation())
            validationDto.setProjectLoanId(dto.getProjectLoanId())
            validationDto.setProspectId(dto.getProspectId())
            validationDto.setFileTypeId(dto.getFileTypeId())
            validationDto.setDetails(responseDetailsDto)
            listValidationDto.add(validationDto)
        })
        dtoRequest.setElements(listValidationDto)
        dtoRequest.setUuid(id)
        dtoRequest.setInitial_time(timeInitial)
        dtoRequest.setNumber_elements(request.size())
        return dtoRequest
    }
}