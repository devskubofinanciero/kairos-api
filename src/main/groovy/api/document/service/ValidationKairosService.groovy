package api.document.service


import api.document.models.ResponseEnroledToGalleryDTO
import mx.com.kubofinanciero.onboarding.common.dto.load.KairosValidationDTO

interface ValidationKairosService {
    String sendMessageToProcess(List<KairosValidationDTO> request)
    ResponseEnroledToGalleryDTO enrrollToGallery(KairosValidationDTO request, Map mapAreApprovedOverGallery)
}