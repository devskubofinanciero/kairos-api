package api.document.models

import lombok.Data

@Data
class KairosMessageAndResponseDTO {
    String uuid
    List<KairosValidationRequestDTO> elements
    Long number_elements
    String initial_time
    String finish_time
    String message
}