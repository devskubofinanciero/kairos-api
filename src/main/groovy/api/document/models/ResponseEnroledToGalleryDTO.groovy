package api.document.models

import lombok.Data

@Data
class ResponseEnroledToGalleryDTO {
    boolean enroll
    String gallery_name
    String message

    ResponseEnroledToGalleryDTO(boolean enroll, String gallery_name, String message) {
        this.enroll = enroll
        this.gallery_name = gallery_name
        this.message = message
    }

    ResponseEnroledToGalleryDTO() {}
}