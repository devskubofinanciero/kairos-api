package api.document.models

import com.fasterxml.jackson.annotation.JsonFormat
import groovy.util.logging.Slf4j

import java.time.LocalDate

@Slf4j
class KairosFaceDetecDTO {
  Long kairosDetectId
  Long companyId
  Long prospectusId
  Long proyectLoanId
  Long fileTypeId
  @JsonFormat(
          pattern = "yyyy-MM-dd"
  )
  LocalDate consultingDate
  String gallerySearch
  String jsonResponse
  String statusResponse
  String hasCoincidences
  Long numberCoincidences
  String coincidences
  String kairosLocation
  String kuboLocation

  KairosFaceDetecDTO(List result) {
    kairosDetectId = result[0]
    companyId = result[1]
    prospectusId = result[2]
    proyectLoanId = result[3]
    fileTypeId = result[4]
    consultingDate = result[5].toLocalDate()
    gallerySearch = result[6]
    jsonResponse= result[7]
    statusResponse= result[8]
    hasCoincidences= result[9]
    numberCoincidences= result[10]
    coincidences= result[11]
    kairosLocation= result[12]
    kuboLocation= result[13]
  }

  KairosFaceDetecDTO(){}
}
