package api.document.models

import lombok.Data

@Data
class DetailEnrollRecognizeDTO {
    String galleryName
    Boolean is_success
    Long time_milis
    String message
    String processDate

    DetailEnrollRecognizeDTO(String galleryName, boolean is_success, long time_milis, String message, String processDate) {
        this.galleryName = galleryName
        this.is_success = is_success
        this.time_milis = time_milis
        this.message = message
        this.processDate = processDate
    }

    DetailEnrollRecognizeDTO(){}
}
