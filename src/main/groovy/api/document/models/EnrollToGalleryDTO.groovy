package api.document.models

import lombok.Data
import mx.com.kubofinanciero.onboarding.common.dto.load.KairosValidationDTO

import javax.validation.constraints.NotNull

@Data
class EnrollToGalleryDTO extends KairosValidationDTO {
    @NotNull
    boolean gallery
}
