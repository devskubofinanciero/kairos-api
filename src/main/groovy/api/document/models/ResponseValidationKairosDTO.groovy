package api.document.models

import lombok.Data

@Data
class ResponseValidationKairosDTO {
    boolean success
    String id
    String message

    ResponseValidationKairosDTO(boolean success, String id, String message) {
        this.success = success
        this.id = id
        this.message = message
    }

    ResponseValidationKairosDTO() {}
}