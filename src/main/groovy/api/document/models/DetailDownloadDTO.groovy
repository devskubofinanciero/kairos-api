package api.document.models

import lombok.Data
import org.codehaus.jackson.annotate.JsonIgnore
import org.codehaus.jackson.annotate.JsonIgnoreProperties
import org.codehaus.jackson.annotate.JsonProperty

@Data
class DetailDownloadDTO{
    String filename
    String fileSize
    Boolean is_success
    Long time_milis
    String message
    String processDate

    DetailDownloadDTO(String filename, String fileSize, boolean is_success, long time_milis, String message, String processDate) {
        this.filename = filename
        this.fileSize = fileSize
        this.is_success = is_success
        this.time_milis = time_milis
        this.message = message
        this.processDate = processDate
    }

    DetailDownloadDTO(){}

    @Override
    String toString(){
        return "DetailDownloadDTO filename: " + filename + " fileSize: " + fileSize + " is_success: " + is_success + " time_milis: " + time_milis + " message: " +message + " processDate: "+ processDate
    }
}
