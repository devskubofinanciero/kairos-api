package api.document.models

import lombok.Data
import mx.com.kubofinanciero.onboarding.common.dto.kairos.KairosFaceDetectDTO
import mx.com.kubofinanciero.onboarding.common.dto.load.KairosValidationDTO

@Data
class KairosValidationRequestDTO extends KairosValidationDTO {
    List<KairosFaceDetectDTO> response
    ResponseDetailsDTO details
    String nameAdaptationForKairos
    Map<String, Boolean> mapAreApprovedOverGallery

    @Override
     String toString() {
        return "KairosValidationDTO ProspectId:" + prospectId + " FileTypeId:" + fileTypeId + " LoanId:" + projectLoanId + " Location:" + fileLocation + "response "  + response + " details "+ details
    }
}