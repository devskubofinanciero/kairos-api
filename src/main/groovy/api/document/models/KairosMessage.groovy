package api.document.models

import groovy.transform.ToString
import javax.validation.Valid
import javax.validation.constraints.NotNull

@ToString
class KairosMessage implements Serializable {
    @NotNull @Valid
    KairosMessageAndResponseDTO kairosValidationDTO
    Integer maxAttempts=2
}
