package api

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication

@SpringBootApplication
class ValidationsKairos {
	static void main(String[] args) {
		SpringApplication.run(ValidationsKairos, args)
	}
}