package api.document.constant

class KairosConstant {
        final static String ENROLL_TO_GALLERY = 'is_enroll'
        final static long  MEGABYTE = 1048576
        final static int DOWNLOAD_QUEUE = 0
        final static int ENROLL_QUEUE = 1
        final static int RECOGNIZE_QUEUE = 2
        final static int  MAX_ATTEMPTS_NUMBER = 2
        final static String  MAX_ATTEMPTS =  "maxAttempts"
        final static String  KAIROS_VALIDATION_DTO = "kairosValidationDTO"
        final static String MAP_ARE_APPROVED_OVER_GALLERY =  "mapAreApprovedOverGallery"
        final static String NAME_ADAPTATION_FOR_KAIROS =  "nameAdaptationForKairos"
}