package api.document.controller

import api.document.exceptions.KairosRequestValidException
import api.document.models.DetailDownloadDTO
import api.document.models.DetailEnrollRecognizeDTO
import api.document.models.KairosMessageAndResponseDTO
import api.document.models.KairosValidationRequestDTO
import api.document.models.ResponseDetailsDTO
import mx.com.kubofinanciero.onboarding.common.dto.load.KairosValidationDTO

import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

class ValidationKairoPMOController {

    protected void requestValidations(List<KairosValidationDTO> request){
        int count = 1
        List<Map<String,Object>> listValid = new ArrayList<>()
        request.forEach({dto ->
            Map<String, Object> mapValidations = validations(dto as KairosValidationDTO, count)
            if(!mapValidations.isEmpty()){
                listValid.add(mapValidations)
            }
            count++})

        if(!listValid.isEmpty()){
            throw new KairosRequestValidException("Parameter list error  ${listValid.toString()}")
        }
    }

    protected Map<String, Object> validations(KairosValidationDTO req, int position) {
        Map<String, Object> body = new LinkedHashMap<>()
        if (req.getProspectId() == null || req.getFileTypeId() == null || req.getProjectLoanId() == null) {
            String prospectIdNull = req.getProspectId() == null ? " ,Error: prospectId can't be null" : ""
            String fileTypeIdNull = req.getFileTypeId() == null ? " ,Error: fileTypeId can't be null" : ""
            String projectLoanIdNull = req.getProjectLoanId() == null ? " ,Error: projectLoanId can't be null" : ""

            body.put("status", 400)
            body.put("error", "Bad Request")
            body.put("exception", "org.springframework.web.bind.MethodArgumentNotValidException")
            body.put("message", "Validation failed for object='kairosValidationDTO' position in List: " + position + prospectIdNull + fileTypeIdNull + projectLoanIdNull)
        }
        body
    }
}
