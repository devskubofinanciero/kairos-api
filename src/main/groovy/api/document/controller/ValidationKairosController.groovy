package api.document.controller

import api.document.models.EnrollToGalleryDTO
import api.document.models.KairosFaceDetecDTO
import api.document.models.KairosMessageAndResponseDTO
import api.document.models.ResponseEnroledToGalleryDTO
import api.document.models.ResponseValidationKairosDTO
import api.document.repositories.KairosFaceDetectRepository
import api.document.service.ValidationKairosService
import groovy.util.logging.Slf4j
import mx.com.kubofinanciero.onboarding.common.dto.load.KairosValidationDTO
import org.codehaus.jackson.map.ObjectMapper
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

import javax.validation.Valid

@Slf4j
@RestController
@RequestMapping("kairos")
class ValidationKairosController extends ValidationKairoPMOController{
    @Autowired
    private ValidationKairosService service

    @Autowired
    private KairosFaceDetectRepository repository

    @PostMapping('file/validation')
    ResponseEntity<Object> kairosValidation(@RequestBody List<KairosValidationDTO> request) {
        requestValidations(request)
       String id =  service.sendMessageToProcess(request)
        ResponseValidationKairosDTO response = new ResponseValidationKairosDTO(true, id, "Request in queue, your request will be attended")
        new ResponseEntity<>(response, HttpStatus.OK)
    }

    @GetMapping("{prospect}")
    ResponseEntity<Object> getProspect(@PathVariable Long prospect) {
        List<KairosFaceDetecDTO> body = repository.findByProspectusId(prospect, null)
        new ResponseEntity<>(body, HttpStatus.OK)
    }

    @GetMapping("prospect/{prospect}/loan/{loan}")
    ResponseEntity<Object> getProspectusLoan(@PathVariable Long prospect, @PathVariable Long loan) {
            List<KairosFaceDetecDTO> body = repository.findByProspectusId(prospect, loan)
        new ResponseEntity<>(body, HttpStatus.OK)
    }

    @PostMapping("enroll/gallery")
    ResponseEntity<Object> enroll(@Valid @RequestBody KairosValidationDTO request) {
        ResponseEnroledToGalleryDTO body = service.enrrollToGallery(request, [:])
        new ResponseEntity<>(body, HttpStatus.OK)
    }
}