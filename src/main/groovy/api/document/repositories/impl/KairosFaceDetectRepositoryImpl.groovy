package api.document.repositories.impl

import api.document.exceptions.KairosGetException
import api.document.models.KairosFaceDetecDTO
import api.document.repositories.KairosFaceDetectRepository
import groovy.util.logging.Slf4j
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Component
import org.springframework.web.server.ResponseStatusException

import javax.persistence.EntityManager
import javax.persistence.NoResultException
import javax.persistence.PersistenceContext
import javax.persistence.Query

@Slf4j
@Component
class KairosFaceDetectRepositoryImpl implements KairosFaceDetectRepository {

    @PersistenceContext
    private EntityManager em

    @Override
    List<KairosFaceDetecDTO> findByProspectusId(Long prospectusId, Long projectLoanId) {
        Query query = getQuery(prospectusId, projectLoanId)
        try {
            List result = query.getResultList()
            if(result.size() < 1){
                throw new KairosGetException("The prospectus does not exist")
            }
            return listToDto(result)
        } catch (NoResultException e) {
            log.error("Error getting Kairos validation data ${e.message}")
            throw new ResponseStatusException(HttpStatus.CONFLICT, "Service Unavailable")
        }
    }

    private List<KairosFaceDetecDTO> listToDto(List result) {
        List<KairosFaceDetecDTO> response = new ArrayList<>()
        result.forEach({ list -> response.add(new KairosFaceDetecDTO(list as List)) })
        return response
    }

    private Query getQuery(Long prospectusId, Long projectLoanId) {
        if(null == projectLoanId) {
            em.createNativeQuery("""SELECT gnkairos.*
                               FROM 
                                  gn_kairos_face_detect gnkairos 
                               WHERE gnkairos.prospectus_id = :prospectusId
                               ORDER BY gnkairos.consulting_date DESC""")
                    .setParameter("prospectusId", prospectusId)
        }else{
            em.createNativeQuery("""SELECT gnkairos.*
                               FROM 
                                  gn_kairos_face_detect gnkairos 
                               WHERE gnkairos.prospectus_id = :prospectusId
                                 AND gnkairos.proyect_loan_id = :projectLoanId
                                 ORDER BY gnkairos.consulting_date DESC""")
                    .setParameter("prospectusId", prospectusId)
                    .setParameter("projectLoanId", projectLoanId)
        }
    }
}
