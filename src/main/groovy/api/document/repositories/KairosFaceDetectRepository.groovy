package api.document.repositories

import api.document.models.KairosFaceDetecDTO

interface KairosFaceDetectRepository {
    List<KairosFaceDetecDTO>  findByProspectusId (Long prospectusId, Long projectLoanId)
}