package api.document.configuration

import api.document.constant.KairosConstant
import groovy.util.logging.Slf4j
import mx.com.kubo.messaging.commons.config.MessagingCommonsConfig
import mx.com.kubofinanciero.client.aws.conf.AwsKuboConfig
import mx.com.kubofinanciero.client.kairos.conf.KairosKuboConfig
import mx.com.kubofinanciero.conf.ClientApiConfig
import mx.com.kubofinanciero.onboarding.common.config.CommonConfig
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Import

@Slf4j
@Configuration
@Import([CommonConfig, AwsKuboConfig, KairosKuboConfig, ClientApiConfig, MessagingCommonsConfig])
class ClientConfiguration {

    @Value('${kubo.storage.local.dynamic.path}') String LOCAL_STORAGE_DYNAMIC_PATH;
    @Value('${kubo.storage.local.static.path}') String LOCAL_STORAGE_STATIC_PATH;
    @Value('${kubo.takes.ids.of.gallery.to.recognize}') private String kuboKeysOfDocuments
    @Value('${kairos.app.gallery.recognize.galleries}') private String recognizes

    @Bean
    @Qualifier("createFolders")
    void createFolders(){
        log.info "created static and dynamic folder"
        new File(LOCAL_STORAGE_DYNAMIC_PATH).mkdir();
        new File(LOCAL_STORAGE_STATIC_PATH).mkdir();
    }

    @Bean
    @Qualifier("keysOfDocuments")
    List<Integer> keysOfDocuments() {
        List<Integer> list = []
        kuboKeysOfDocuments.split(",").each {
            list.add(it.split(":")[1] as Integer)
        }
        log.info "created ${list.toString()}"
        list
    }

    @Bean
    @Qualifier("createInicialMapToApproved")
    Map<String, Boolean> createInicialMapToApproved() {
        Map<String, Boolean> map = [:]
        map.put((KairosConstant.ENROLL_TO_GALLERY), false)
        List<String> list = recognizes.split('::')
        list.each { gallery ->
            map.put(gallery, false)
        }
        log.info "created ${map.toString()}"
        map
    }

}