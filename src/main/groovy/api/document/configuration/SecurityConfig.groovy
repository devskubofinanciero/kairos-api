package api.document.configuration

import mx.com.kubo.commons.oauth.config.OAuthResourceGlobalConfig
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Import

@Configuration
@Import([OAuthResourceGlobalConfig])
class SecurityConfig {
}
