package api.document.configuration

import org.springframework.boot.autoconfigure.domain.EntityScan
import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.Configuration
import org.springframework.data.jpa.repository.config.EnableJpaRepositories

@Configuration
@ComponentScan(basePackages = [
  'mx.com.kubo.microfin.commons.service',
  'mx.com.kubo.microfin.commons.repository',
  'mx.com.kubo.microfin.commons.management.closingFlag'
])
@EntityScan(basePackages = [
  'mx.com.kubo.microfin.commons.entities',
  'mx.com.kubo.financiero.transfer.entities',
  'mx.com.kubo.microfin.commons.management.closingFlag'
])
@EnableJpaRepositories(basePackages = [
  'mx.com.kubo.financiero.transfer.repositories',
  'mx.com.kubo.microfin.commons.repository',
  'mx.com.kubo.microfin.commons.management.closingFlag'
])
class MicrofinCommons {
}