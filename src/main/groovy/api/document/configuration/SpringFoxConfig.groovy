package api.document.configuration

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter
import springfox.documentation.builders.ApiInfoBuilder
import springfox.documentation.builders.PathSelectors
import springfox.documentation.builders.RequestHandlerSelectors
import springfox.documentation.service.ApiInfo
import springfox.documentation.spi.DocumentationType
import springfox.documentation.spring.web.plugins.Docket
import springfox.documentation.swagger2.annotations.EnableSwagger2

@SuppressWarnings("deprecation")
@Configuration
@EnableSwagger2
class SpringFoxConfig  extends WebMvcConfigurerAdapter  {

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("swagger-ui.html").addResourceLocations("classpath:/META-INF/resources/");
        registry.addResourceHandler("/webjars/**").addResourceLocations("classpath:/META-INF/resources/webjars/");
    }

    @Bean
    Docket api() {
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.basePackage('api.document.controller'))
                .paths(PathSelectors.ant('/**'))
                .build()
                .apiInfo(getApiInfo())
                .ignoredParameterTypes(groovy.lang.MetaClass.class)
    }

    ApiInfo getApiInfo(){
        new ApiInfoBuilder()
                .title('Kairos API')
                .description('Microservice API for Kairos')
                .version('1.0.1')
                .build()
    }

}