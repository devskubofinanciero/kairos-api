package api.document.configuration

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.SerializationFeature
import mx.com.kubo.messaging.commons.config.CustomRabbitConnection
import org.aopalliance.aop.Advice
import org.springframework.amqp.core.*
import org.springframework.amqp.rabbit.annotation.RabbitListenerConfigurer
import org.springframework.amqp.rabbit.config.RetryInterceptorBuilder
import org.springframework.amqp.rabbit.config.SimpleRabbitListenerContainerFactory
import org.springframework.amqp.rabbit.connection.ConnectionFactory
import org.springframework.amqp.rabbit.core.RabbitTemplate
import org.springframework.amqp.rabbit.listener.RabbitListenerEndpointRegistrar
import org.springframework.amqp.rabbit.retry.RejectAndDontRequeueRecoverer
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Primary
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder
import org.springframework.retry.interceptor.RetryOperationsInterceptor

@Configuration
class RabbitConfig extends CustomRabbitConnection implements RabbitListenerConfigurer {

  @Value('${spring.rabbitmq.virtualHostKairosApi:/}') private String virtualHostKairosApi

  @Value('${kubo.onboarding.kairos.queue}') private String KAIROS_QUEUE
  @Value('${kubo.onboarding.kairos.queue.enroll}') private String KAIROS_QUEUE_ENROLL
  @Value('${kubo.onboarding.kairos.queue.recognize}') private String KAIROS_QUEUE_RECOGNIZE
  @Value('${kubo.onboarding.kairos.exchange}') private String KAIROS_EXCHANGE
  @Value('${kubo.onboarding.kairos.exchange.enroll}') private String KAIROS_EXCHANGE_ENROLL
  @Value('${kubo.onboarding.kairos.exchange.recognize}') private String KAIROS_EXCHANGE_RECOGNIZE
  @Value('${kubo.onboarding.kairos.dead-letter}') private String KAIROS_DLQ_QUEUE

  @Value('${kubo.onboarding.kairos.queue.result}') private String KAIROS_RESULT
  @Value('${kubo.onboarding.kairos.exchange.result}') private String KAIROS_EXCHANGE_RESULT

  @Bean
  @Qualifier('kairosQueue')
  Queue kairosQueue() {
    QueueBuilder
            .durable(KAIROS_QUEUE)
            .withArgument('x-dead-letter-exchange', '')
            .withArgument('x-dead-letter-routing-key', KAIROS_DLQ_QUEUE)
            .build()
  }

  @Bean
  @Qualifier('kairosQueueEnroll')
  Queue kairosQueueEnroll() {
    QueueBuilder
            .durable(KAIROS_QUEUE_ENROLL)
            .withArgument('x-dead-letter-exchange', '')
            .withArgument('x-dead-letter-routing-key', KAIROS_DLQ_QUEUE)
            .build()
  }

  @Bean
  @Qualifier('kairosQueueRecognize')
  Queue kairosQueueRecognize() {
    QueueBuilder
            .durable(KAIROS_QUEUE_RECOGNIZE)
            .withArgument('x-dead-letter-exchange', '')
            .withArgument('x-dead-letter-routing-key', KAIROS_DLQ_QUEUE)
            .build()
  }

  @Bean
  @Qualifier('kairosQueueResult')
  Queue kairosQueueResult() {
    QueueBuilder
            .durable(KAIROS_RESULT)
            .withArgument('x-dead-letter-exchange', '')
            .withArgument('x-dead-letter-routing-key', KAIROS_DLQ_QUEUE)
            .build()
  }

  @Bean
  @Qualifier('kairosExchangeResult')
  DirectExchange kairosExchangeResult() {
    new DirectExchange(KAIROS_EXCHANGE_RESULT)
  }

  @Bean
  Binding bindingKairosResult(@Qualifier('kairosQueueResult') Queue kairosQueueResult,
                        @Qualifier('kairosExchangeResult') DirectExchange kairosExchangeResult) {
    BindingBuilder
            .bind(kairosQueueResult)
            .to(kairosExchangeResult)
            .with(KAIROS_RESULT)
  }

  @Bean
  @Qualifier('kairosExchange')
  DirectExchange kairosExchange() {
    new DirectExchange(KAIROS_EXCHANGE)
  }

  @Bean
  @Qualifier('kairosExchangeEnroll')
  DirectExchange kairosExchangeEnroll() {
    new DirectExchange(KAIROS_EXCHANGE_ENROLL)
  }

  @Bean
  @Qualifier('kairosExchangeRecognize')
  DirectExchange kairosExchangeRecognize() {
    new DirectExchange(KAIROS_EXCHANGE_RECOGNIZE)
  }


  @Bean
  Binding bindingKairos(@Qualifier('kairosQueue') Queue kairosQueue,
                                    @Qualifier('kairosExchange') DirectExchange kairosExchange) {
    BindingBuilder
            .bind(kairosQueue)
            .to(kairosExchange)
            .with(KAIROS_QUEUE)
  }

  @Bean
  Binding bindingKairosEnroll(@Qualifier('kairosQueueEnroll') Queue kairosQueue,
                        @Qualifier('kairosExchangeEnroll') DirectExchange kairosExchange) {
    BindingBuilder
            .bind(kairosQueueEnroll())
            .to(kairosExchangeEnroll())
            .with(KAIROS_QUEUE_ENROLL)
  }

  @Bean
  Binding bindingKairosRecognize(@Qualifier('kairosQueueRecognize') Queue kairosQueue,
                        @Qualifier('kairosExchangeRecognize') DirectExchange kairosExchange) {
    BindingBuilder
            .bind(kairosQueueRecognize())
            .to(kairosExchangeRecognize())
            .with(KAIROS_QUEUE_RECOGNIZE)
  }

  @Bean
  @Primary
  ObjectMapper objectMapper(Jackson2ObjectMapperBuilder builder) {
    ObjectMapper objectMapper = builder.createXmlMapper(false).build()
    objectMapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false)
    objectMapper
  }

  @Bean
  @Qualifier('producerMessageConverter')
  Jackson2JsonMessageConverter producerMessageConverter(ObjectMapper objectMapper) {
    new Jackson2JsonMessageConverter(objectMapper)
  }

  @Bean
  @Primary
  @Qualifier('kairosConnectionFactory')
  ConnectionFactory kairosConnectionFactory() {
    getCachingConnectionFactory(virtualHostKairosApi)
  }

  @Primary
  @Bean
  @Qualifier('kairosRabbitTemplate')
  RabbitTemplate kairosRabbitTemplate(@Qualifier('kairosConnectionFactory') ConnectionFactory connectionFactory) {
    RabbitTemplate template = new RabbitTemplate(connectionFactory)
    template.setMessageConverter(producerMessageConverter())
    template
  }

  @Bean
  SimpleRabbitListenerContainerFactory kairosFactory(@Qualifier('kairosConnectionFactory') ConnectionFactory connectionFactory) {
    final SimpleRabbitListenerContainerFactory factory = new SimpleRabbitListenerContainerFactory()
    factory.setConnectionFactory(connectionFactory)
    factory.setMessageConverter(producerMessageConverter())
    factory.setConcurrentConsumers(4);
    factory.setMaxConcurrentConsumers(4);
    factory.setAdviceChain([retries()] as Advice[]);
    factory
  }

  @Bean
   RetryOperationsInterceptor retries() {
    return RetryInterceptorBuilder.stateless()
            .maxAttempts(2)
            .backOffOptions(1000, 1.0, 10000)
            .recoverer(new RejectAndDontRequeueRecoverer()).build();
  }

  @Override
  void configureRabbitListeners(RabbitListenerEndpointRegistrar registrar) {
    registrar
  }
}